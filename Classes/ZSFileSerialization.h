//
//  FileSerialization.h
//  WeSchoolStudent
//  文件夹、文件操作 序列化类
//  Created by safiri on 15/9/23.
//  Copyright © 2015年 safiri. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface ZSFileSerialization : NSObject

//MARK: - 路径操作
//from UIApplication+YYAdd

/// "Documents" folder in this app's sandbox.
+ (NSURL *)documentsURL;
+ (NSString *)documentsPath;

/// "Caches" folder in this app's sandbox.
+ (NSURL *)cachesURL;
+ (NSString *)cachesPath;

/// "Library" folder in this app's sandbox.
+ (NSURL *)libraryURL;
+ (NSString *)libraryPath;

/// 应用程序路径 -> NSHomeDirectory();
/// Temp路径 -> NSTemporaryDirectory();

// 获得磁盘上最常用文件夹的路径
// -> [NSSearchPathForDirectoriesInDomains(searchPath, domainMask, YES) firstObject];
// 创建文件路径
// -> [[self libraryPath] stringByAppendingPathComponent:fileName];

/**
 根据文件名创建Documents文件路径
 @param fileName  格式list.txt
 @return 文件路径
 */
+ (NSString *)fileCreatePathInDocumentsWithFileName:(NSString *)fileName;
/**
 根据文件名创建Library文件路径
 @param fileName  格式list.txt
 @return 文件路径
 */
+ (NSString *)fileCreatePathInLibraryWithFileName:(NSString *)fileName;

/**
 根据文件名创建Caches文件路径
 @param fileName  格式list.txt
 @return 文件路径
 */
+ (NSString *)fileCreatePathInCachesWithFileName:(NSString *)fileName;

//MARK: - 文件操作
/**
 判断文件
 - (BOOL)fileExistsAtPath:(NSString *)path;
 - (BOOL)fileExistsAtPath:(NSString *)path isDirectory:(nullable BOOL *)isDirectory;
 - (BOOL)isReadableFileAtPath:(NSString *)path;
 - (BOOL)isWritableFileAtPath:(NSString *)path;
 - (BOOL)isExecutableFileAtPath:(NSString *)path;
 - (BOOL)isDeletableFileAtPath:(NSString *)path;
 */

/// 从URL路径中移除文件
+ (BOOL)fileRemoveOfURL:(NSURL *)fileURL;
/// 从Path路径中移除文件
+ (BOOL)fileRemoveOfPath:(NSString *)filePath;
/// 创建目录(文件夹) 
+ (BOOL)fileCreatDirectoryWithPath:(NSString *)directoryPath;
/// 创建文件
+ (BOOL)fileCreatWithPath:(NSString *)filePath;
/// 移动文件
+ (BOOL)fileMoveFromPath:(NSString *)fromFilePath toPath:(NSString *)toFilePath;
/// 拷贝文件
+ (BOOL)fileCopyFromPath:(NSString *)fromFilePath toPath:(NSString *)toFilePath;
/// 获取文件夹下文件列表
+ (NSArray *)fileGetFileListInDirectoryPath:(NSString *)directoryPath;
/// 获取文件大小
+ (long long)fileGetSizeWithPath:(NSString *)filePath;
/// 获取文件创建时间
+ (NSString *)fileGetCreatDateWithPath:(NSString *)filePath;
/// 获取文件所有者
+ (NSString *)fileGetOwnerWithPath:(NSString *)filePath;
/// 获取文件更改日期
+ (NSString *)fileGetChangeDateWithPath:(NSString *)filePath;

// MARK: - 文件数据操作
/**
 保存文件数据
 @param filePath 存储路径
 @param data 文件数据
 @return 是否成功
 */
+ (BOOL)fileSaveData:(NSData *)data withPath:(NSString *)filePath;
/**
 追加写文件
 @param filePath 存储路径
 @param data 文件数据
 @return 是否成功
 */
+ (BOOL)fileAppendData:(NSData *)data withPath:(NSString *)filePath;
/// 获取文件全部数据
+ (NSData *)fileGetData:(NSString *)filePath;
/**
 获取文件数据
 @param filePath 文件路径
 @param startIndex 文件数据获取位置
 @param length 文件数据获取长度
 @return 获取到的文件数据
 */
+ (NSData *)fileGetData:(NSString *)filePath startIndex:(long long)startIndex length:(NSInteger)length;


@end
NS_ASSUME_NONNULL_END

/*
 tips:
 App所产生的数据都存在于自己的沙盒中，一般沙盒都有3个文件：Documents、Library和tmp。
 Documents：这个目录存放用户数据。存放用户可以管理的文件；iTunes备份和恢复的时候会包括此目录。
 Library:主要使用它的子文件夹,我们熟悉的NSUserDefaults就存在于它的子目录中。
 Library/Caches:存放缓存文件，iTunes不会备份此目录，此目录下文件不会在应用退出删除,“删除缓存”一般指的就是清除此目录下的文件。
 Library/Preferences:NSUserDefaults的数据存放于此目录下。
 tmp:App应当负责在不需要使用的时候清理这些文件，系统在App不运行的时候也可能清理这个目录。
 */
