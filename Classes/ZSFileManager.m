//
//  ZSFileManager.m
//  EPayment
//
//  Created by safiri on 16/4/15.
//  Copyright © 2016年 com.egintra. All rights reserved.
//

#import "ZSFileManager.h"

@implementation ZSFileManager

+ (NSString *)sandboxDocumentsPathWithFileName:(NSString *)fileName {
    NSString *file = [fileName stringByAppendingPathExtension:@"archiver"];
    NSString *filePath = [ZSFileSerialization fileCreatePathInDocumentsWithFileName:file];
    return filePath;
}


+ (BOOL)cashDataObject:(id)dataObject withFileName:(NSString *)fileName {
    NSString *filePath = [ZSFileManager sandboxDocumentsPathWithFileName:fileName];
    if ([dataObject respondsToSelector:@selector(writeToFile:atomically:)]) {
        return [dataObject writeToFile:filePath atomically:YES];
    }else{
        return NO;
    }
}
+ (NSArray *)getArrayWithFileName:(NSString *)fileName {
    NSString *filePath = [ZSFileManager sandboxDocumentsPathWithFileName:fileName];
    return [NSArray arrayWithContentsOfFile:filePath];
}
+ (NSDictionary *)getDictionaryWithFileName:(NSString *)fileName {
    NSString *filePath = [ZSFileManager sandboxDocumentsPathWithFileName:fileName];
    return [NSDictionary dictionaryWithContentsOfFile:filePath];
}
+ (NSString *)getStringWithFileName:(NSString *)fileName {
    NSString *filePath = [ZSFileManager sandboxDocumentsPathWithFileName:fileName];
    return [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
}

+ (NSData *)getDataWithFileName:(NSString *)fileName {
    NSString *filePath = [ZSFileManager sandboxDocumentsPathWithFileName:fileName];
    return [NSData dataWithContentsOfFile:filePath];
}

+ (BOOL)archiverObject:(id)dataObject inSandboxDocumentsWithFileName:(NSString *)fileName {
    return [NSKeyedArchiver archiveRootObject:dataObject toFile:[ZSFileManager sandboxDocumentsPathWithFileName:fileName]];
}
+ (id)unarchiverObjectWithFileName:(NSString *)fileName {
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[ZSFileManager sandboxDocumentsPathWithFileName:fileName]];
}

+ (BOOL)deleteArchiverObjectWithFileName:(NSString *)fileName {
    return [ZSFileSerialization fileRemoveOfPath:[ZSFileManager sandboxDocumentsPathWithFileName:fileName]];
}


#pragma mark - custom filepath

//归档存放文件名
//#define ArchiverFile  @"myObj.archiver"
//归档文件中存放的对象key
#define ArchiveKeyForUser  @"ArchiveKeyForUser"

//自定义归档
+ (BOOL)archiverObject:(id)object inFilePath:(NSString *)filePath withKey:(NSString *)key {
    //自定义内容归档 - 使用键值对
    NSMutableData *data = [[NSMutableData alloc]init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:object forKey:key];
    [archiver finishEncoding];
    
    return [data writeToFile:filePath atomically:YES];
    
}
//自定义解档
+ (id)unarchiverObjectFromFilePath:(NSString *)filePath withKey:(NSString *)key {
    NSData *contentData = [NSData dataWithContentsOfFile:filePath];
    NSKeyedUnarchiver *unarchive = [[NSKeyedUnarchiver alloc] initForReadingWithData:contentData];
    id object = [unarchive decodeObjectForKey:ArchiveKeyForUser];
    return object;
}

@end
