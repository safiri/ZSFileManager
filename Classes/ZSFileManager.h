//
//  ZSFileManager.h
//  EPayment
//
//  Created by safiri on 16/4/15.
//  Copyright © 2016年 com.egintra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSFileSerialization.h"
#import "ZSArchiveObjBase.h"

NS_ASSUME_NONNULL_BEGIN
@interface ZSFileManager : NSObject

+ (NSString *)sandboxDocumentsPathWithFileName:(NSString *)fileName;
// MARK: writeToFile
+ (BOOL)cashDataObject:(id)dataObject withFileName:(NSString *)fileName;
+ (nullable NSArray *)getArrayWithFileName:(NSString *)fileName;
+ (nullable NSDictionary *)getDictionaryWithFileName:(NSString *)fileName;
+ (nullable NSString *)getStringWithFileName:(NSString *)fileName;
+ (nullable NSData *)getDataWithFileName:(NSString *)fileName;

// MARK: - Archiver
+ (BOOL)archiverObject:(id)dataObject inSandboxDocumentsWithFileName:(NSString *)fileName;
+ (nullable id)unarchiverObjectWithFileName:(NSString *)fileName;
+ (BOOL)deleteArchiverObjectWithFileName:(NSString *)fileName;
/**
 *  归档 -> [NSKeyedArchiver archiveRootObject:obj toFile:filePath];
 */

/**
 *  删除 -> [self archiveRootObject:nil toFile:path];
 */

/**
 *  解档 ->[NSKeyedUnarchiver unarchiveObjectWithFile:path];
 */


/**
 * 自定义归档
 */
+ (BOOL)archiverObject:(id)object inFilePath:(NSString *)filePath withKey:(NSString *)key;

/**
 * 自定义解档
 */
+ (nullable id)unarchiverObjectFromFilePath:(NSString *)filePath withKey:(NSString *)key;
@end
NS_ASSUME_NONNULL_END
