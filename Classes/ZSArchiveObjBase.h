//
//  ZSArchiveObjBase.h
//  AllToolsOC
//
//  Created by safiri on 2017/8/8.
//  Copyright © 2017年 safiri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSArchiveObjBase : NSObject<NSCoding,NSCopying>

- (BOOL)writeToSandboxDocumentsPathWithFileNameWithFileName:(NSString *)fileName;
- (BOOL)writeToFilePath:(NSString *)filePath;

+ (id)loadFromSandboxDocumentsWithFileName:(NSString *)fileName;
+ (id)loadFromFilePath:(NSString *)path;

@end
