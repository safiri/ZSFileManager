# ZSFileManager

#### 项目介绍

文件管理工具类:

`ZSFileSerialization`: 文件夹/文件路径操作、文件操作、文件数据操作

`ZSArchiveObjBase`: 实现序列化协议 `NSCoding`,`NSCopying`-自定义类继承无需再实现；
					提供写入DocumentsPath的实例方法
					提供从DocumentsPath读取对象实例的类方法

`ZSFileManager`: 提供系统类(`NSArray/NSDictionary/NSString`)在DocumentsPath中的写入与读取;
				 简单的自定义归档和解档实现

#### 安装教程

pod 'ZSFileManager'